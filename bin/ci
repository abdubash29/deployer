#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

HELP="Usage: bin/ci **kwargs [lint|helm|command|deploy]"

chatreply() {
  echo -e "section_start:$( date +%s ):chat_reply\r\033[0K\n${1:-}\nsection_end:$( date +%s ):chat_reply\r\033[0K"
}

COMMAND="${1:-}"
shift

case "$COMMAND" in

  lint)

    yamllint .
    ansible-lint playbook.yml
    ansible-playbook playbook.yml --inventory inventory/local --syntax-check

    ;;

  helm)

    helm upgrade --install --namespace deployer deployer --values runner/values.yaml runner/chart

    ;;

  command)

    COMMAND=$1
    shift

    REF=main
    COLOR=
    SHARD=

    while [[ "$#" -gt 0 ]]; do
      echo "arg: $1"
      case $1 in
        blue) COLOR=blue ;;
        green) COLOR=green ;;

        private) SHARD=private ;;
        shared-gitlab-org) SHARD=shared-gitlab-org ;;
        shared) SHARD=shared ;;

        -r|--ref) REF="${2:-}"; [ -z "${2:-}" ] || shift ;;

        *) echo "Invalid argument: $1"; exit 1 ;;
      esac
      shift
    done

    if [ -z "$COMMAND" ]; then
      chatreply "Available commands: \`ping\`, \`start\`, \`stop\`, \`chef\`. Example: \`/runner run ping private blue\`"
      exit 0
    fi

    if [ -z "$COLOR" ] || [ -z "$SHARD" ]; then
      chatreply "Must specify a color and shard. Example: \`/runner run ping private blue\`"
      exit 0
    fi

    if [ -z "$REF" ]; then
      chatreply "Use --ref to specify a git reference. Defaults to \`main\` if ommitted. Example: \`/runner run ping private blue --ref testing-branch\`"
      exit 0
    fi

    trap 'rm -f "$TMPFILE"' EXIT
    TMPFILE=$(mktemp)

    echo "Triggerring pipeline..."
    curl --silent --request POST \
      --header "PRIVATE-TOKEN: $GITLAB_PROJECT_ACCESS_TOKEN" \
      --header "Content-Type: application/json" \
      --data '{ "ref": "'$REF'", "variables": [ {"key": "DEPLOYER_COLOR", "value": "'$COLOR'"}, {"key": "DEPLOYER_SHARD", "value": "'$SHARD'"}, {"key": "DEPLOYER_COMMAND", "value": "'$COMMAND'"} ] }'\
      https://ops.gitlab.net/api/v4/projects/518/pipeline | tee "$TMPFILE"; echo ""

    PIPELINE_ID=$(jq -r '.id' "$TMPFILE")
    if [ "$PIPELINE_ID" == "null" ]; then
      echo "Triggering pipeline failed"
      exit 1
    fi

    echo "Getting deploy job"
    curl --silent --request GET \
      --header "PRIVATE-TOKEN: $GITLAB_PROJECT_ACCESS_TOKEN" \
      https://ops.gitlab.net/api/v4/projects/518/pipelines/$PIPELINE_ID/jobs | tee "$TMPFILE"; echo ""

    JOB_URL=$(jq -r 'map(select(.name=="deploy"))[0].web_url' "$TMPFILE")
    if [ "$JOB_URL" == "null" ]; then
      echo "Unable to find triggered job"
      exit 1
    fi

    DEPLOYMENT_DASH="https://dashboards.gitlab.net/d/ci-runners-deployment/ci-runners-deployment-overview?orgId=1&var-environment=gprd&var-shard=$SHARD&refresh=10s&from=now-30m&to=now"

    REF_MSG=
    if [ "$REF" != "main" ]; then
      REF_MSG=" (on ref \`$REF\`)"
    fi

    chatreply ":rocket: Executing \`$COMMAND\` on \`$SHARD\`, \`$COLOR\`$REF_MSG...     <$JOB_URL|:gitlab: CI Job>     <$DEPLOYMENT_DASH|:grafana: Deployment dashboard>"

    ;;

  deploy)

    if [ -z "$DEPLOYER_COLOR" ] || [ -z "$DEPLOYER_SHARD" ] || [ -z "$DEPLOYER_COMMAND" ]; then
      echo "Must specify a command, a color and a shard."
      exit 1
    fi

    # login to google
    gcloud auth activate-service-account --key-file "$GCP_SERVICE_ACCOUNT_FILE"

    # add temporary SSH key
    ssh-keygen -t rsa -C "runners-deployer pipeline $CI_PIPELINE_ID job $CI_JOB_ID" -f ./deployer_rsa -N ""
    gcloud compute os-login ssh-keys add --key-file=./deployer_rsa.pub --ttl=24h > /dev/null
    USERNAME=$(gcloud compute os-login describe-profile --format "value(posixAccounts[0].username)")

    # start monitoring progress
    if [ "$DEPLOYER_COMMAND" == "stop" ]; then
      gcloud compute instances list --format="value(name,networkInterfaces[0].networkIP)" --filter="labels.deployment=$DEPLOYER_COLOR AND labels.shard=$DEPLOYER_SHARD" | \
      parallel --colsep '\t' -j10 --line-buffer --tagstring '\033[30;3{=1 $_=($job->seq()+1)%8 =}m{=1 $_=`date -Isec`; chomp =} {1}' --verbose "cat bin/progress | ssh -i ./deployer_rsa -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $USERNAME@{2} bash -" &
    fi

    # Update inventory with settings for CI container
    sed -i "s/  ansible_host:.*/  ansible_host: networkInterfaces[0].networkIP/" inventory/gcp.yml
    sed -i "s/service_account_file:/# service_account_file: /" inventory/gcp.yml

    # execute ansible playbook
    ansible-playbook --inventory inventory/gcp.yml --private-key ./deployer_rsa --user "$USERNAME" --limit "gcp_deployment_${DEPLOYER_COLOR//-/_}:&gcp_shard_${DEPLOYER_SHARD//-/_}" --tags "$DEPLOYER_COMMAND" playbook.yml

    if [ "$DEPLOYER_COMMAND" == "stop" ]; then
      kill %1
    fi

    ;;

  *)

    echo -e "$HELP"
    exit 1

    ;;

esac
